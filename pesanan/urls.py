from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    # path('create/', create_view, name='create'),
    path('create-pesanan/', create_pesanan, name='create-pesanan'),
    path('add-new-pesanan/', add_new_pesanan, name='add-new-pesanan'),
    # path('update-pesanan/', update_pesanan, name='update-pesanan'),
    # path('delete-pesanan/', delete_pesanan, name='delete-pesanan'),
    path('daftar-pesanan/', daftar_pesanan, name='daftar-pesanan'),
]
