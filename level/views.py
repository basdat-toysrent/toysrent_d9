from django.shortcuts import render
from .models import Level
from django.db import connection
from django.http import HttpResponseRedirect


# Create your views here.

def create_level(request):
    return render(request, "create_level.html")

def add_new_level(request):
    nama_level=request.POST.get('nama_level_form')
    min_poin=request.POST.get('min_poin_form')
    deskripsi=request.POST.get('deskripsi_form')

    cursor=connection.cursor()
    cursor.execute('SET SEARCH_PATH TO TOYSRENT,public')

    # Add to db
    values="'"+nama_level+"','"+min_poin+"','"+deskripsi+"'"
    query="INSERT INTO LEVEL_KEANGGOTAAN values (" +values+ ")"
    cursor.execute(query)
    return HttpResponseRedirect("/daftar-level/")

def daftar_level(request):
    response={}
    with connection.cursor() as cursor:
        cursor.execute("select * from level_keanggotaan")
        response['levels'] = cursor.fetchall()
    return render(request, "daftar_level.html", response)

