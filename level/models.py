from django.db import models

# Create your models here.
class Level(models.Model):
    nama_level = models.CharField(max_length=20)
    poin_min = models.FloatField()
    dekripsi = models.TextField()