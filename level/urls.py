from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    # path('create/', create_view, name='create'),
    path('create-level/', create_level, name='create-level'),
    path('add-new-level/', add_new_level, name='add-new-level'),
    # path('update-level/', update_level, name='update-level'),
    # path('delete-level/', delete_level, name='delete-level'),
    path('daftar-level/', daftar_level, name='daftar-level'),
]
