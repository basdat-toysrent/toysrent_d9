from django import forms

class Add_Level_Form(forms.Form):
    namaLevel = forms.CharField(label="Nama Level", required=True,max_length = 20)
    minimalPoin = forms.IntegerField(label="Poin Minimal", required = True)
    deskripsi = forms.TextField(label = "Deskripsi")
    