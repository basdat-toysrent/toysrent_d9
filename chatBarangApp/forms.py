from django import forms

class Create_Barang_From(forms.Form):
    idbarang = forms.CharField(label = "id", required = True, max_length = 100, widget=forms.TextInput(attrs= {'class' : 'form-control'}))
    namaItem = forms.CharField(label = "Nama", required = True, min_length = 8, widget = forms.TextInput(attrs = {'class' : 'form-control'}))