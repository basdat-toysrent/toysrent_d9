from django.shortcuts import render
from .forms import *

# Create your views here.


def showChatAdminHTML(request):
    return render(request, 'chatBarangApp/chat.html')


def showChatAnggotaHTML(request):
    return render(request, 'chatBarangApp/chatAnggota.html')


def showBarangHTML(request):
    formCreate = Create_Barang_From()
    return render(request,'chatBarangApp/barangUpdateDeleteAdd.html', {"formC":formCreate})

def showDaftarBarangHTML(request):
    return render(request,'chatBarangApp/daftarbarang.html')


