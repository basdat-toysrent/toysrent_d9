from django.urls import path
from . import views

urlpatterns = [
    path('chatAdmin/', views.showChatAdminHTML, name = 'chatAdmin' ),
    path('chatAnggota/', views.showChatAnggotaHTML, name = 'chatAnggota'), 
    path('barangCreate/',views.showBarangHTML, name = 'barang'),
 
]