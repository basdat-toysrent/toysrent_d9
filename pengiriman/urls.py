from django.contrib import admin
from django.urls import path
from .views import *
urlpatterns = [
    path('create-anggota', createAnggota_view, name='createShipmentAnggota'),
    path('create-admin', createAdmin_view, name='createShipmentAdmin'),
    path('update-anggota', updateAnggota_view, name='updateShipmentAnggota'),
    path('update-admin', updateAdmin_view, name='updateShipmentAdmin'),
    path('review', review_view, name='review'),
    path('update-review', updateReview_view, name='updateReview'),
    path('list-pengiriman-all', listPengirimanSemua_view, name='listPengirimanSemua'),
    path('list-pengiriman-anggota', listPengirimanAnggota_view, name='listPengirimanAnggota'),
    path('list-review', listReview_view, name='listReview'),
    path('home/', home_view, name='home')
]
