import uuid

from django.db import connection
from django.shortcuts import render, redirect
from .forms import *
from .models import *

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def home_view(request):
    return render(request, "home.html")

def createAnggota_view(request):
    context = {}
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama_item FROM BARANG")
        row = dictfetchall(cursor)
        context['barangs'] = row
        cursor.execute("SELECT nama FROM ALAMAT")
        row = dictfetchall(cursor)
        context['allAlamat'] = row

    return render(request, "createAnggota.html", context=context)

def createAdmin_view(request):
    context = {}
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama_item FROM BARANG")
        row = dictfetchall(cursor)
        context['barangs'] = row
        cursor.execute("SELECT nama FROM ALAMAT")
        row = dictfetchall(cursor)
        context['allAlamat'] = row

    return render(request, "createAdmin.html", context=context)

def updateAnggota_view(request):
    return render(request, "updateAnggota.html")

def updateAdmin_view(request):
    return render(request, "updateAdmin.html")

def review_view(request):
    context = {}
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama_item FROM BARANG")
        row = dictfetchall(cursor)
        context['barangs'] = row

    return render(request, "reviewAnggota.html", context=context)

def listPengirimanSemua_view(request):
    return render(request, "listPengirimanSemua.html")

def listPengirimanAnggota_view(request):
    return render(request, "listPengirimanAnggota.html")

def listReview_view(request):
    return render(request, "listReview.html")

def updateReview_view(request):
    return render(request, "updateReview.html")
# def create_view(request):
# 	form = FormBarang_create(request.POST or None)
# 	response = {}
# 	if(request.method == "POST" and form.is_valid()):
# 		barang = request.POST['barang']
# 		peminjaman_create.objects.create(
# 			barang = barang
# 		)
# 		alamat = request.POST['alamat']
# 		peminjaman_create.objects.create(
# 			alamat = alamat
# 		)
# 		tanggal = request.POST.get("tanggal")
# 		metode = request.POST.get("metode")
# 		peminjaman_create.objects.create(barang = barang, alamat = alamat, tanggal = tanggal, metode = metode)
# 		return redirect('/daftar/')

# 	# vouchers = voucher_create.objects.all().order_by("-id")
# 	response = {
# 	    "form" : form,
# 	}
# 	return render(request, 'create.html', response)

# def update_view(request):
# 	form = FormBarang_update(request.POST or None)
# 	response = {}
# 	if(request.method == "POST" and form.is_valid()):
# 		barang = request.POST.get("barang")
# 		alamat = request.POST.get("alamat")
# 		tanggal = request.POST.get("tanggal")
# 		metode = request.POST.get("metode")
# 		peminjaman_create.objects.create(barang = barang, alamat = alamat, tanggal = tanggal, metode = metode)
# 		return redirect('/voucher/')

# 	# vouchers = voucher_create.objects.all().order_by("-id")
# 	# response = {
# 	#     "vouchers" : vouchers,
# 	#     "form" : form,
# 	# }
# 	return render(request, 'update.html', response)

