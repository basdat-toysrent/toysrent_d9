from django.db import models

class peminjaman_create(models.Model):
	PILIHAN_BARANG = (
  		('Barang 1', 'Barang 1'),
  		('Barang 2', 'Barang 2')
 	)

	barang = models.CharField(
		max_length = 10,
		choices = PILIHAN_BARANG,
		default = 'Barang 1'
	)

	PILIHAN_ALAMAT = (
		('Alamat 1', 'Alamat 1'),
		('Alamat 2', 'Alamat 2')
	)

	alamat = models.CharField(
		max_length = 10,
		choices = PILIHAN_ALAMAT,
		default = 'Alamat 1'
	)	
	tanggal = models.CharField(max_length=50)
	metode = models.CharField(max_length=50)

class peminjaman_update(models.Model):
	barang = models.CharField(max_length=50)
	alamat = models.CharField(max_length=50)
	tanggal = models.CharField(max_length=50)
	metode = models.CharField(max_length=50)