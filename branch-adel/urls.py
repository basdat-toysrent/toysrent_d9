from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('home/', home_view, name='this_is_main_page'),

    path('radmin/', reg_admin_page, name='register_as_admin'),
    path('rmember/', reg_member_page, name='register_as_member'),

    path('ladmin/', login_admin_page, name='login_as_admin'),
    path('lmember/', login_member_page, name='login_as_member'),

    path('barang-list/', barang_list_page, name='admin_initial_page'),

    path('admin-profile/', admin_profile_page, name='admin_profile'),
    path('member-profile/', member_profile_page, name='member_profile'),

    path('admin-item-list/', admin_item_list_page, name='admin_item_list_page'),
    path('member-item-list/', member_item_list_page, name='member_item_list_page'),
    path('add-item/', add_item_page, name='add_item_page'),
    path('success-add-item/', insert_item, name='success_add_item'),

    path('anggota_barang_list/', anggota_barang_list, name='barang_list_anggota'),
    
    path('ladmin-process/', login_admin_process, name='login_admin_process'),
    path('lmember-process/', login_member_process, name='login_member_process'),
    path('logout/', logout, name='logout'),
    path('radmin-process/', reg_admin_insert, name='register_as_admin_process'),
    path('rmember-process/', reg_member_insert, name='register_as_member_process'),
]