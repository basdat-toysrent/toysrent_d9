from django.core.validators import RegexValidator
from django import forms
from .models import RAdmin

class register_admin_form(forms.ModelForm):
    no_ktp = forms.CharField(widget=forms.TextInput(attrs={
        'class':'form-control',
        'required':True,
    }), label='Your SSN', max_length=20)
    
    nama_lengkap = forms.CharField(widget=forms.TextInput(attrs={
        'class':'form-control',
        'required':True,
    }), label='Your Full Name', max_length=225)
    
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class':'form-control',
        'required':True,
    }), label='Your EMail', label='E-mail')
    
    tanggal_lahir = forms.DateField(widget=forms.DateInput(attrs={
        'class':'form-control',
        'required':True,
        'placeholder':'YYYY-MM-DD',
    }), label='Your Birth Date', max_length=225)

    no_telp = forms.IntegerField(widget=forms.TextInput(attrs={
        'class':'form-control',
        'required':True,
    }), label='Your Telephone Number', max_length=20)
    
    class Meta:
        model = Member
        fields = ('no_ktp', 'nama_lengkap', 'email', 'tanggal_lahir', 'no_telp')
