from django.db import models

class RAdmin(models.Model):
    no_ktp = models.CharField(max_length=20)
    nama_lengkap = models.CharField(max_length=225)
    email = models.EmailField(unique=True, max_length=225)
    tanggal_lahir = models.DateField()
    no_telp = models.CharField(max_length=200)

    def __str__(self):
        return self.username