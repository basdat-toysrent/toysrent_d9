from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.utils.datastructures import MultiValueDictKeyError

def home_view(request):
    return render(request, "home.html")
	
def reg_admin_page(request):
    return render(request, "registerAdmin.html")

def reg_admin_insert(request):
    no_ktp=request.POST['no_ktp']
    # Cek No KTP
    if(len(no_ktp) != 11):
        messages.error(request, "Pastikan bahwa No KTP terdiri dari 9 digit angka dan 2 setrip sesuai dengan format!")
        return HttpResponseRedirect('/radmin/')
    else:
        nama=request.POST['nama_lengkap']
        email=request.POST['email']
        tgl_lahir=request.POST['tgl_lahir']
        if(tgl_lahir==""):
            tgl_lahir=None

        no_telp=request.POST['no_telp']
        if(no_telp==""):
            no_telp=None

        # Cek validasi No KTP belum terdaftar
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO TOYSRENT")
        cursor.execute("SELECT * FROM ADMIN WHERE no_ktp='"+no_ktp+"'")
        result=cursor.fetchone()

        if (result):
            if len(result)>0:
                messages.error(request, "No KTP "+no_ktp+" sudah pernah terdaftar. Silakan gunakan No KTP lain.")
                return HttpResponseRedirect("/radmin/")
        
        # Cek validasi Email belum terdaftar
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO TOYSRENT")
        cursor.execute("SELECT * FROM PENGGUNA WHERE email='"+email+"'")
        result=cursor.fetchone()

        if (result):
            if len(result)>0:
                messages.error(request, "Email "+email+" sudah pernah terdaftar. Silakan gunakan email lain.")
                return HttpResponseRedirect("/radmin/")
        
        # Tambah data admin ke tabel PENGGUNA
        values="'"+no_ktp+"','"+nama+"','"+email+"','"+tgl_lahir+"','"+no_telp+"'"
        query="INSERT INTO PENGGUNA (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp) values (" +values+ ")"
        cursor.execute(query)
        
        # Tambah data admin ke tabel ADMIN
        values="'"+no_ktp+"'"
        query="INSERT INTO ADMIN (no_ktp) values (" + values+ ")"
        cursor.execute(query)
        
        # Pesan sukses registrasi
        messages.success(request, "SELAMAT! Registrasi Anda Berhasil, "+nama+
        "!\nAnda secara resmi merupakan bagian dari ToysRent." + 
        "\nTerima kasih telah mempercayai kami dan have a good journey💖💖")
        return HttpResponseRedirect("/radmin/")

def reg_member_page(request):
    return render(request, "registerMember.html")

def reg_member_insert(request):
    no_ktp=request.POST['no_ktp']
    kode_pos=request.POST['kode_pos']
    # Cek No KTP
    if(len(no_ktp) != 11):
        messages.error(request, "Pastikan bahwa No KTP terdiri dari 9 digit angka dan 2 setrip sesuai dengan format!")
        return HttpResponseRedirect("/rmember/")

    # Cek Kode Pos
    elif (len(kode_pos) > 10 or len(kode_pos) <= 0):
        messages.error(request, "Pastikan bahwa Kode Pos terdiri minimal 4 maksimal 10 digit!")
        return HttpResponseRedirect("/rmember/")
    else:
        nama=request.POST['nama_lengkap']
        email=request.POST['email']
        tgl_lahir=request.POST['tgl_lahir']
        no_telp=request.POST['no_telp']
        jenis_alamat=request.POST['jenis_alamat']
        jalan=request.POST['jalan']
        no_jalan=request.POST['no_jalan']
        kota=request.POST['kota']
        kode_pos=request.POST['kode_pos']
    
        # Cek validasi No KTP belum terdaftar
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO TOYSRENT")
        cursor.execute("SELECT * FROM ANGGOTA WHERE no_ktp='"+no_ktp+"'")
        result=cursor.fetchone()
        if (result):
            if len(result)>0:
                messages.error(request, "No KTP "+no_ktp+" sudah pernah terdaftar.")
                return HttpResponseRedirect("/rmember/")
        
        # Cek validasi Email belum terdaftar
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO TOYSRENT")
        cursor.execute("SELECT * FROM PENGGUNA WHERE email='"+email+"'")
        result=cursor.fetchone()
        if (result):
            if len(result)>0:
                messages.error(request, "Email "+email+" sudah pernah terdaftar.")
                return HttpResponseRedirect("/rmember/")

        # Tambah data anggota ke tabel PENGGUNA
        values="'"+no_ktp+"','"+nama+"','"+email+"','"+tgl_lahir+"','"+no_telp+"'"
        query="INSERT INTO PENGGUNA (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) values (" +values+ ")"
        cursor.execute(query)

        # Tambah data anggota ke tabel ANGGOTA
        default_poin="200" # Asumsi default poin 200 karena minimum poin untuk Bronze kami adalah 200
        default_level=""
        values="'"+no_ktp+"','"+default_poin+"'"
        query="INSERT INTO ANGGOTA (no_ktp, poin) values (" + values + ")"
        cursor.execute(query)

        # Tambah data alamat anggota ke tabel ALAMAT
        values="'"+no_ktp+"','"+nama+"','"+jalan+"','"+no_jalan+"','"+kota+"','"+kode_pos+"'"
        query="INSERT INTO ALAMAT (no_ktp,nama,jalan,nomor,kota,kodepos) values (" +values+ ")"
        cursor.execute(query)
        
        # Pesan sukses registrasi
        messages.success(request, "SELAMAT! Registrasi Anda Berhasil, "+nama+
        "!\nAnda secara resmi merupakan bagian dari ToysRent." + 
        "\nTerima kasih telah mempercayai kami dan have a good journey💖💖")
        return HttpResponseRedirect("/rmember/")

def login_admin_page(request):
    if ('no_ktp' not in request.session.keys()):
        return HttpResponseRedirect('/ladmin/')
    return render(request, "loginAdmin.html")
	
def login_member_page(request):
    if ('no_ktp' not in request.session.keys()):
        return HttpResponseRedirect('/lmember/')
    return render(request, "loginMember.html")

@csrf_exempt
def login_admin_process(request):
    if request.method == 'POST':
        no_ktp=request.POST['no_ktp']
        email=request.POST['email']
       
        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO TOYSRENT,public")
        cursor.execute("SELECT * FROM PENGGUNA WHERE no_ktp='"+no_ktp+"' and email='"+email+"'")
        result=cursor.fetchone()
        if (result):
            cursor.execute("SELECT * FROM ADMIN WHERE no_ktp='"+no_ktp+"'")
            result_admin=cursor.fetchone()
            if(result_admin):
                cursor.execute("SELECT nama_lengkap FROM PENGGUNA WHERE no_ktp='"+no_ktp+"'")
                nama_admin=cursor.fetchall()
                for i in nama_admin:
                    nama_admin=i[0]
                request.session['nama_lengkap'] = nama_admin
                request.session['no_ktp'] = no_ktp
                request.session['email'] = email
                # request.session['role'] = 'admin'
                return HttpResponseRedirect("/daftar-pesanan/")
        else:
            messages.error(request, "HEY ADMIN! Pastikan Anda memasukkan Email dan No KTP yang sesuai!")
            return HttpResponseRedirect("/ladmin/")

@csrf_exempt
def login_member_process(request):
    if request.method == 'POST':
        no_ktp=request.POST['no_ktp']
        email=request.POST['email']

        cursor=connection.cursor()
        cursor.execute("SET SEARCH_PATH TO TOYSRENT,public")
        cursor.execute("SELECT * FROM PENGGUNA WHERE no_ktp='"+no_ktp+"' and email='"+email+"'")
        result=cursor.fetchone()
        if (result):
            cursor.execute("SELECT * FROM ANGGOTA WHERE no_ktp='"+no_ktp+"'")
            result_member=cursor.fetchone()
            if(result_member):
                cursor.execute("SELECT nama_lengkap FROM PENGGUNA WHERE no_ktp='"+no_ktp+"'")
                nama_member=cursor.fetchall()
                for i in nama_member:
                    nama_member=i[0]
                request.session['nama_lengkap'] = nama_member
                request.session['no_ktp'] = no_ktp
                request.session['email'] = email
                # request.session['role'] = 'anggota'
                return HttpResponseRedirect("/anggota_barang_list/")
        else:
            messages.error(request, "HEY ANGGOTA! Pastikan Anda memasukkan Email dan No KTP yang sesuai!")
            return HttpResponseRedirect("/lmember/")

def logout(request):
    request.session.flush()
    return render(request, "home.html")
	
def order_list_page(request):
    return render(request, "order-list.html")
	
def admin_profile_page(request):
    no_ktp=request.session['no_ktp']
    cursor=connection.cursor()
    cursor.execute("SET SEARCH_PATH TO TOYSRENT,public")
    cursor.execute("SELECT * FROM PENGGUNA WHERE no_ktp='"+no_ktp+"'")
    
    response={}
    response['data'] = cursor.fetchall()
    return render(request, "admin_profile.html", response)

def barang_list_page(request):
    return render(request, "barang-list.html")
	
def member_profile_page(request):
    response={}
    no_ktp=request.session['no_ktp']
    cursor=connection.cursor()
    cursor.execute("SET SEARCH_PATH TO TOYSRENT,public")
    cursor.execute("SELECT * FROM PENGGUNA WHERE no_ktp='"+no_ktp+"'")
    response['pengguna']=cursor.fetchall()

    cursor.execute("SELECT * FROM ALAMAT WHERE no_ktp_anggota='"+no_ktp+"'")
    response['alamat']=cursor.fetchall()

    cursor.execute("SELECT * FROM ANGGOTA WHERE no_ktp='"+no_ktp+"'")
    response['poin_level']=cursor.fetchall()
    return render(request, "member-profile.html", response)
	
def admin_item_list_page(request):
    response={}
    cursor=connection.cursor()
    cursor.execute("SET SEARCH_PATH TO TOYSRENT,public")
    cursor.execute("SELECT * FROM KATEGORI_ITEM")
    response["item_kategori"]=cursor.fetchall()
    return render(request, "admin-item-list.html", response)
	
def member_item_list_page(request):
    response={}
    cursor=connection.cursor()
    cursor.execute("SET SEARCH_PATH TO TOYSRENT,public")
    cursor.execute("SELECT * FROM KATEGORI_ITEM")
    response["item_kategori"]=cursor.fetchall()
    return render(request, "member-item-list.html", response)
	
def add_item_page(request):
    # Daftar kategori untuk dropdown field Kategori
    response={}
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM KATEGORI")
        response["kategori"]=cursor.fetchall()
    return render(request, "add-item.html", response)

@csrf_exempt
def insert_item(request):
    nama_item=request.POST['nama_item']
    deskripsi=request.POST['deskripsi']
    usia_dari=request.POST['usia_dari']
    usia_sampai=request.POST['usia_sampai']  
    bahan=request.POST['bahan']
    nama_kategori=request.POST['kategori']
    
    cursor=connection.cursor()
    cursor.execute('SET SEARCH_PATH TO TOYSRENT,public')

    # Tambah data item anggota ke tabel ITEM
    values="'"+nama_item+"','"+deskripsi+"','"+usia_dari+"','"+usia_sampai+"','"+bahan+"'"
    query="INSERT INTO ITEM (nama, deskripsi, usia_dari, usia_sampai, bahan) values (" +values+ ")"
    cursor.execute(query)

    # Tambah data item & kategori anggota ke tabel KATEGORI_ITEM
    values="'"+nama_item+"','"+nama_kategori+"'"
    query="INSERT INTO KATEGORI_ITEM (nama_item, nama_kategori) values (" +values+ ")"
    cursor.execute(query)

    return HttpResponseRedirect("/admin-item-list/")
	
def anggota_barang_list(request):
    # Daftar barang
    response={}
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM BARANG")
        response["barang"]=cursor.fetchall()
    return render(request, "anggota-barang-list.html", response)